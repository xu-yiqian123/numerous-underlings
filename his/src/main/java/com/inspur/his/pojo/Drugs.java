package com.inspur.his.pojo;

import java.util.Date;

/**
 * 药品实体类
 */
public class Drugs {
    private int drugs_id;//药品id
    private String drugs_name;//药品的名字
    private float drugs_in_price;//药品的进价
    private float drugs_sale_price;//药品的卖价
    private int drugs_num;//药品的数量
    private Date drugs_in_date;//药品的引入日期
    private String drugs_manufacturer;//药品的生产厂商
    private Date drugs_create_date;//药品的生产日期
    private Date drugs_quality_date;//药品的保质期
    private String drugs_supplier;//药品的供货单位
    private int drugs_addnum;//药品新增

    public int getDrugs_id() {
        return drugs_id;
    }

    public void setDrugs_id(int drugs_id) {
        this.drugs_id = drugs_id;
    }

    public String getDrugs_name() {
        return drugs_name;
    }

    public void setDrugs_name(String drugs_name) {
        this.drugs_name = drugs_name;
    }

    public float getDrugs_in_price() {
        return drugs_in_price;
    }

    public void setDrugs_in_price(float drugs_in_price) {
        this.drugs_in_price = drugs_in_price;
    }

    public float getDrugs_sale_price() {
        return drugs_sale_price;
    }

    public void setDrugs_sale_price(float drugs_sale_price) {
        this.drugs_sale_price = drugs_sale_price;
    }

    public int getDrugs_num() {
        return drugs_num;
    }

    public void setDrugs_num(int drugs_num) {
        this.drugs_num = drugs_num;
    }

    public Date getDrugs_in_date() {
        return drugs_in_date;
    }

    public void setDrugs_in_date(Date drugs_in_date) {
        this.drugs_in_date = drugs_in_date;
    }

    public String getDrugs_manufacturer() {
        return drugs_manufacturer;
    }

    public void setDrugs_manufacturer(String drugs_manufacturer) {
        this.drugs_manufacturer = drugs_manufacturer;
    }

    public Date getDrugs_create_date() {
        return drugs_create_date;
    }

    public void setDrugs_create_date(Date drugs_create_date) {
        this.drugs_create_date = drugs_create_date;
    }

    public Date getDrugs_quality_date() {
        return drugs_quality_date;
    }

    public void setDrugs_quality_date(Date drugs_quality_date) {
        this.drugs_quality_date = drugs_quality_date;
    }

    public String getDrugs_supplier() {
        return drugs_supplier;
    }

    public void setDrugs_supplier(String drugs_supplier) {
        this.drugs_supplier = drugs_supplier;
    }

    public int getDrugs_addnum() {
        return drugs_addnum;
    }

    public void setDrugs_addnum(int drugs_addnum) {
        this.drugs_addnum = drugs_addnum;
    }

    @Override
    public String toString() {
        return "drugs{" +
                "drugs_id=" + drugs_id +
                ", drugs_name='" + drugs_name + '\'' +
                ", drugs_in_price=" + drugs_in_price +
                ", drugs_sale_price=" + drugs_sale_price +
                ", drugs_num=" + drugs_num +
                ", drugs_in_date=" + drugs_in_date +
                ", drugs_manufacturer='" + drugs_manufacturer + '\'' +
                ", drugs_create_date=" + drugs_create_date +
                ", drugs_quality_date=" + drugs_quality_date +
                ", drugs_supplier='" + drugs_supplier + '\'' +
                ", drugs_addnum=" + drugs_addnum +
                '}';
    }
}
