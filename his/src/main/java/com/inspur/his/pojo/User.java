package com.inspur.his.pojo;

import javax.xml.crypto.Data;
import java.util.Date;

/**
 * 用户信息实体类
 */
public class User {
    private String id;
    private String username;
    private String password;
    private String role;
    private String realname;
    private String sex;
    private int  age;
    private String tel;
    private String address;
    private Date cteatedate;
    private String status;



    public String getId() {
        return id;
    }

    public Date getCteatedate() {
        return cteatedate;
    }

    public void setCteatedate(Date cteatedate) {
        this.cteatedate = cteatedate;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }

    public String getRealname() {
        return realname;
    }

    public String getSex() {
        return sex;
    }

    public int getAge() {
        return age;
    }

    public String getTel() {
        return tel;
    }

    public String getAddress() {
        return address;
    }



    public String getStatus() {
        return status;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public void setAddress(String address) {
        this.address = address;
    }



    public void setStatus(String status) {
        this.status = status;
    }
}
