package com.inspur.his.mapper;

import com.inspur.his.pojo.Dept;
import com.inspur.his.pojo.DeptExt;

import java.util.HashMap;
import java.util.List;

public interface DeptMapper {
    public List<DeptExt> deptList(HashMap map);//科室查询

    void deptAdd(Dept dept);

    Dept deptById(Dept dept);

    void deptUpdate(Dept dept);
}
