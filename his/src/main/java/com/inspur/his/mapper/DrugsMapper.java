package com.inspur.his.mapper;

import com.inspur.his.pojo.Drugs;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
public interface DrugsMapper {
    public List<Drugs> drugsList(HashMap map);//药品查询

    public void drugsAdd(Drugs drugs);//药品添加

    void drugsDelById(HashMap map);//删除药品


    Drugs drugsById(HashMap hashMap);//根据药品id查询药品信息

    void drugsUpdate(Drugs drugs);//修改药品信息

    void drugsUpdateNum(Drugs drugs);//新增药品数量
}
