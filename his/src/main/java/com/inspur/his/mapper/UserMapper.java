package com.inspur.his.mapper;

import com.inspur.his.pojo.User;
import org.springframework.stereotype.Repository;


import java.util.HashMap;
import java.util.List;

@Repository
public interface UserMapper {
    /**
     * 登录的方法
     * @param map
     * @return
     */
    public User getUserByNameAndPass(HashMap map);
/**
 * 密码修改
 */
  public int userpwdupdate(HashMap map);

    /**
     * 根据id修改密码
     * @param
     * @param
     * @return
     */
  public User checkPwdById(HashMap map);

    /**
     * 根据条件查询列表
     * @param hashMap
     * @return
     */
    public List<User> userList(HashMap hashMap);

    /**
     * 增加用户
     * @param
     */
    public void addUser(User user);

  /**
   * 增加用户时判断，该用户名和密码是否已经存在
   * @return
   */
  User checkPwd(HashMap hashMap);

  /**
   * 根据id查询用户
   * @param id
   * @return
   */
  public User getUserById(String id);

  /**
   * 修改用户信息
   * @param user
   */
  public void updateUser(User user);

  /**
   * 修改用户的状态
   * @param map
   */

    void changeStatus(HashMap map);
}
