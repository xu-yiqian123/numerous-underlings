package com.inspur.his.controller;

import com.github.pagehelper.Constant;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.inspur.his.pojo.User;
import com.inspur.his.service.UserService;
import com.inspur.his.until.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.xml.crypto.Data;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Controller
public class UserlistController {
    @Autowired
    UserService userService;

    @RequestMapping("userlistController")
    public String userList(HttpServletRequest request, Model model, @RequestParam(value = "pn",defaultValue = "1") Integer pn){
        //获取页面查询的条件
        String id =request.getParameter("id");
        String username = request.getParameter("username");
        String role = request.getParameter("role");
        //查询用户信息
        HashMap hashMap = new HashMap();
        hashMap.put("id",id);
        hashMap.put("username",username);
        hashMap.put("role",role);
        //分页查询查询第几页，一页显示几条
        PageHelper.startPage(pn,5);
        List<User> userList = userService.userList(hashMap);
        //将默认的查询列表转换为pageinfo类型的对象
        PageInfo<User> pageInfo = new PageInfo<User>(userList,userList.size());
        //封装数据
        model.addAttribute("userlist",userList);
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute("id",id);
        model.addAttribute("username",username);
        model.addAttribute("srole",role);
        return "/user/userList.jsp";
    }

    /**
     * 添加用户
     * @param
     * @return
     */
    @RequestMapping("addUserController")
    public String addUser(User user,Model model) throws ParseException {
        String pwd=user.getPassword();
        user.setPassword(MD5Utils.md5(pwd));
        Date day = new Date();
        SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-DD");
        String today = fm.format(day);
        user.setCteatedate(fm.parse(today));
        userService.addUser(user);
        model.addAttribute("user",user);
        model.addAttribute("flag", 1);
        model.addAttribute("success","用户信息保存成功！");
        return "user/userAdd.jsp";

    }

    /**
     * 查询用户名是否唯一
     * @param request
     * @return
     */
    @RequestMapping("checkName")
    @ResponseBody
    public String checkPwdById(HttpServletRequest request){
        String username = request.getParameter("username");
        HashMap hashMap1 = new HashMap();
        hashMap1.put("username",username);
        User user =null;
        user = userService.checkPwd(hashMap1);

        if (user!=null){
            return "1";
        }else return "0";

    }


    /**
     * 修改用户的状态
     * @param request
     * @return
     */
    @RequestMapping("changeStatus")
    public String changeStatus(HttpServletRequest req, Model md,
                               @RequestParam(value = "pn", defaultValue = "1") Integer pn) {
        // 获取页面传递过来的查询条件
        String username = req.getParameter("username");
        String role = req.getParameter("role");
        // 页面查询条件查询后锁定显示在页面上
        md.addAttribute("username", username);
        md.addAttribute("role", role);

        String id = req.getParameter("id");
        String sts = req.getParameter("sts");
        HashMap map = new HashMap<String, String>();
        map.put("id", id);
        map.put("status", sts);
        userService.changeStatus(map);
        return "redirect:userlistController";
    }
    /**
     * 根据id查询信息要修改的信息
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("/modifyUserController")
    public String modifyUser(String id, Model model) {
        User user = userService.getUserById(id);
        model.addAttribute("user", user);
        return "/user/userModify.jsp";
    }

    /**
     * 修改用户信息
     * @param user
     * @param model
     * @return
     */
    @RequestMapping("/userUpdateController")
    public String userupdate(User user, Model model) {
        userService.updateUser(user);
        model.addAttribute("success", "用户信息修改成功");
        model.addAttribute("user", user);
        model.addAttribute("flag", "1");
        return "/user/userModify.jsp";

    }
}
