package com.inspur.his.controller;

import com.inspur.his.pojo.User;
import com.inspur.his.service.UserService;


import com.inspur.his.until.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;

@Controller
public class LoginController {
    @Autowired
    private UserService userService;
    @RequestMapping("loginController")
    public ModelAndView login(String username, String password, HttpSession httpSession){
        ModelAndView model = new ModelAndView();
        HashMap hashMap = new HashMap();
        String pwd = MD5Utils.md5(password);//密码加密
        hashMap.put("username",username);
        hashMap.put("password",pwd);
        User t_user = userService.getUserByNameAndPass(hashMap);
        if(t_user!=null){
            httpSession.setAttribute("t_user",t_user);
            model.addObject("message","欢迎你使用社区医疗信息管理系统！");
            model.setViewName("index.jsp");
        }else{
            model.addObject("error","用户名或密码错误，请重新登录");
            model.setViewName("login.jsp");
        }
        return model;
    }
    @RequestMapping("loginoutController")
    public String logout( HttpSession session){
        session.setAttribute("user",null);
        return  "login.jsp";
    }

}

