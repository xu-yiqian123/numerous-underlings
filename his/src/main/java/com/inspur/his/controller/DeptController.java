package com.inspur.his.controller;


import com.inspur.his.constant.Constant;
import com.inspur.his.pojo.Dept;
import com.inspur.his.pojo.DeptExt;
import com.inspur.his.pojo.User;
import com.inspur.his.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Controller
public class DeptController {
    @Autowired
    private DeptService deptService;

    @RequestMapping("deptListController")
    public String userList(User user, HttpServletRequest req, Model md) {
        String name = req.getParameter("name");
        HashMap map = new HashMap<String, String>();
        map.put("name", name);
        List<DeptExt> deptList = deptService.deptList(map);
        md.addAttribute("deptList", deptList);
        md.addAttribute("name", name);
        return "dept/deptList.jsp";
    }
    @RequestMapping("deptAddController")
    public String deptAdd(Dept dept, Model model) throws Exception {
        //参数绑定 user对象中已经绑定了表单中的用户信息(确认sql中插入的所有字段均有值吗？）
        //思考主键的生成方式--多种1 MySQL中的主键自增
        Date day = new Date();
        dept.setCreatedate(day);
        dept.setStatus(Constant.USER_STATUS_ABLE);
        deptService.deptAdd(dept);
        model.addAttribute("dept",dept);
        model.addAttribute("flag",Constant.USER_STATUS_ABLE);
        model.addAttribute("success","科室信息保存成功！");
        return "dept/deptAdd.jsp";
    }
    @RequestMapping("modifyDeptController")
    public String modifyDeptController(Dept dept,Model model){
        Dept dept1 = deptService.deptById(dept);
        model.addAttribute("dept",dept1);
        return "/dept/deptModify.jsp";
    }
    @RequestMapping("deptUpdateController")
    public String deptUpdateController(Dept dept,Model model){
        deptService.deptUpdate(dept);
        model.addAttribute("success", "用户信息修改成功");
        model.addAttribute("dept", dept);
        model.addAttribute("flag", "1");
        return "/dept/deptModify.jsp";
    }
}

