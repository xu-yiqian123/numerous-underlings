package com.inspur.his.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.inspur.his.pojo.Drugs;
import com.inspur.his.service.DrugsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@Controller
public class DrugsListController {
    @Autowired
    private DrugsService drugsService;
    @RequestMapping("drugsListController")
    public String drugsList(HttpServletRequest request, Model model, @RequestParam(value = "pn",defaultValue = "1") Integer pn){
        //获取页面查询的条件
        String drugs_id =request.getParameter("drugs_id");
        String drugs_name = request.getParameter("drugs_name");
        //查询用户信息
        HashMap hashMap = new HashMap();
        hashMap.put("drugs_id",drugs_id);
        hashMap.put("drugs_name",drugs_name);
        //分页查询查询第几页，一页显示几条
        PageHelper.startPage(pn,5);
        List<Drugs> drugsList = drugsService.drugsList(hashMap);
        //将默认的查询列表转换为pageinfo类型的对象
        PageInfo<Drugs> pageInfo = new PageInfo<Drugs>(drugsList,drugsList.size());
        //封装数据
        model.addAttribute("drugsList",drugsList);
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute("drugs_id",drugs_id);
        model.addAttribute("drugs_name",drugs_name);

        return "/drugs/drugsList.jsp";
    }
    /**
     * 药品增加
     */

    @RequestMapping("addDrugsController")
    public String drugsAdd(Drugs drugs, Model model){
        drugsService.drugsAdd(drugs);
        model.addAttribute("drugs",drugs);
        model.addAttribute("flag", 1);
        model.addAttribute("success","用户信息保存成功！");
        return "/drugs/drugsAdd.jsp";
    }
    /**
     * 删除药品
     */
    @RequestMapping("delDrugsController")
    public String delDrugs(HttpServletRequest req,
                               @RequestParam(value = "pn", defaultValue = "1") Integer pn) {

        String id = req.getParameter("id");
        HashMap map = new HashMap<String, String>();
        map.put("drugs_id", id);
        drugsService.drugsDelById(map);
        return "redirect:drugsListController";
    }
    /**
     * 根据药品id查询药品信息
     */
    @RequestMapping("modifyDrugsController")
    public String modifyDrugs(String id,Model model){
        HashMap hashMap = new HashMap();
        hashMap.put("drugs_id",id);
        Drugs drugs =drugsService.drugsById(hashMap);
        model.addAttribute("drugs",drugs);
        return "/drugs/drugsModify.jsp";
    }
    /**
     * 修改药品的信息
     */
    @RequestMapping("/drugsUpdateController")
    public String drugsUpdate(Drugs drugs, Model model) {
       drugsService.updateDrugs(drugs);
        model.addAttribute("success", "药品信息修改成功");
        model.addAttribute("drugs", drugs);
        model.addAttribute("flag", "1");
        return "/drugs/drugsModify.jsp";

    }
    /**
     *根据药品ID查询药品数量
     */
    @RequestMapping("czDrugsController")
    public String czDrugs(String id,Model model){
        HashMap hashMap = new HashMap();
        hashMap.put("drugs_id",id);
        Drugs drugs =drugsService.drugsById(hashMap);
        model.addAttribute("drugs",drugs);
        return "/drugs/drugsCz.jsp";
    }
    /**
     * 新增药品数量
     */
    @RequestMapping("/drugsUpdateNumController")
    public String drugsUpdateNum(Drugs drugs, Model model) {
        drugsService.updateNumDrugs(drugs);
        model.addAttribute("success", "药品信息修改成功");
        model.addAttribute("drugs", drugs);
        model.addAttribute("flag", "1");
        return "/drugs/drugsCz.jsp";

    }

}
