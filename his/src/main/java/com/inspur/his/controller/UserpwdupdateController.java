package com.inspur.his.controller;

import com.inspur.his.pojo.User;
import com.inspur.his.service.UserService;
import com.inspur.his.until.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


import java.util.HashMap;

@Controller
public class UserpwdupdateController {
    @Autowired
    private UserService userService;
    @RequestMapping("userpwdupdateController")
    public ModelAndView userpwdupdate(String password2, String user_id){
                ModelAndView model = new ModelAndView();


                   HashMap hashMap = new HashMap();
                   String pwd = MD5Utils.md5(password2);//密码加密
                   hashMap.put("id",user_id);
                   hashMap.put("password",pwd);
                   int resout = userService.userpwdupdate(hashMap);
                   if (resout!=0){
                       model.addObject("message","密码修改成功");
                       model.setViewName("success.jsp");
                   }else{
                       model.addObject("error","密码修改失败");
                       model.setViewName("login.jsp");
                   }




           return model;

    }
    @RequestMapping("checkPwdById")
    @ResponseBody
    public String checkPwdById(String id,String password){
        HashMap hashMap1 = new HashMap();
        hashMap1.put("id",id);
        String pwd1 = MD5Utils.md5(password);//密码加密
        hashMap1.put("password",pwd1);
        User user =null;
         user = userService.checkPwdById(hashMap1);

        if (user!=null){
            return "1";
        }else return "0";

    }

}
