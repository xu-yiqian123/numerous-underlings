package com.inspur.his.service;

import com.inspur.his.pojo.User;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

@Resource
public interface UserService {
    /**
     * 登录的方法
     * @param map
     * @return
     */
    public User getUserByNameAndPass(HashMap map);

    /**
     * 修改密码
     * @param map
     * @return
     */
    public int userpwdupdate(HashMap map);

    /**
     * 根据id检查密码是否正确
     * @param
     * @param
     * @return
     */
    public User checkPwdById(HashMap hashMap);
    //查询用户列表
    List<User> userList(HashMap hashMap);
    //增加用户
    public void addUser(User user);

    //添加用户时，判断密码和用户名是否已经存在
    User checkPwd(HashMap hashMap1);
    //根据id查询用户信息
    User getUserById(String id);
    //修改用户信息
    void updateUser(User user);
    //修改用户的状态
    void changeStatus(HashMap map);
}
