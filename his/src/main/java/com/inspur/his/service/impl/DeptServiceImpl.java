package com.inspur.his.service.impl;

import com.inspur.his.mapper.DeptMapper;
import com.inspur.his.pojo.Dept;
import com.inspur.his.pojo.DeptExt;
import com.inspur.his.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
@Service("deptService")
public class DeptServiceImpl implements DeptService {
    @Autowired
    private DeptMapper deptMapper;
    @Override
    public List<DeptExt> deptList(HashMap map) {
        return deptMapper.deptList(map);
    }

    @Override
    public void deptAdd(Dept dept){ deptMapper.deptAdd(dept);}
    @Override
    public Dept deptById(Dept dept) {
        return deptMapper.deptById(dept);
    }

    @Override
    public void deptUpdate(Dept dept) {
        deptMapper.deptUpdate(dept);
    }
}
