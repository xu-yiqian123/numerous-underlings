package com.inspur.his.service.impl;

import com.inspur.his.mapper.UserMapper;
import com.inspur.his.pojo.User;
import com.inspur.his.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public User getUserByNameAndPass(HashMap map) {
        return userMapper.getUserByNameAndPass(map);
    }

    @Override
    public int userpwdupdate(HashMap map) {
        return userMapper.userpwdupdate(map);
    }

    @Override
    public User checkPwdById(HashMap map) {
        return userMapper.checkPwdById(map);
    }

    @Override
    public List<User> userList(HashMap hashMap) {
        return userMapper.userList(hashMap);
    }

    @Override
    public void addUser(User user) {
      userMapper.addUser(user);
    }

    @Override
    public User checkPwd(HashMap hashMap1) {
        return userMapper.checkPwd(hashMap1);
    }

    @Override
    public User getUserById(String id) {
        return userMapper.getUserById(id);
    }

    @Override
    public void updateUser(User user) {
        userMapper.updateUser(user);

    }
    //修改用户状态
    @Override
    public void changeStatus(HashMap map) {
        userMapper.changeStatus(map);
    }


}
