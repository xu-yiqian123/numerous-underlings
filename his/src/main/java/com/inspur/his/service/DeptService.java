package com.inspur.his.service;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import com.inspur.his.pojo.Dept;
import com.inspur.his.pojo.DeptExt;

@Resource
public interface DeptService {

    public List<DeptExt> deptList(HashMap map);//科室查询


    void deptAdd(Dept dept);

    Dept deptById(Dept dept);

    void deptUpdate(Dept dept);
}