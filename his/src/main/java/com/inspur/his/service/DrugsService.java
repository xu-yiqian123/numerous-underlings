package com.inspur.his.service;

import com.inspur.his.pojo.Drugs;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public interface DrugsService {
    public List<Drugs> drugsList(HashMap map);//药品查询

    public void drugsAdd(Drugs drugs);//药品增加

    void drugsDelById(HashMap map);//删除药品

    Drugs drugsById(HashMap hashMap);//根据id查询药品信息

    void updateDrugs(Drugs drugs);//修改药品信息

    void updateNumDrugs(Drugs drugs);//新增药品数量
}
