package com.inspur.his.service.impl;


import com.inspur.his.mapper.DrugsMapper;
import com.inspur.his.pojo.Drugs;
import com.inspur.his.service.DrugsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service("drugsService")
public class DrugsServiceImpl implements DrugsService {
    @Autowired
    private DrugsMapper drugsMapper;
    @Override
    public List<Drugs> drugsList(HashMap map) {
        return drugsMapper.drugsList(map);
    }

    @Override
    public void drugsAdd(Drugs drugs) {
        drugsMapper.drugsAdd(drugs);
    }

    @Override
    public void drugsDelById(HashMap map) {
        drugsMapper.drugsDelById(map);
    }

    @Override
    public Drugs drugsById(HashMap hashMap) {
        return  drugsMapper.drugsById(hashMap);
    }

    @Override
    public void updateDrugs(Drugs drugs) {
        drugsMapper.drugsUpdate(drugs);
    }

    @Override
    public void updateNumDrugs(Drugs drugs) {
        drugsMapper.drugsUpdateNum(drugs);
    }


}
