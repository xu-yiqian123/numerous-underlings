<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>无标题文档</title>
    <link href="<%=basePath%>css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=basePath%>js/jquery.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $('tbody tr:odd').addClass("trLight");

            $(".select-all").click(function(){
                if($(this).attr("checked")){
                    $(".checkBox input[type=checkbox]").each(function(){
                        $(this).attr("checked", true);
                    });
                }else{
                    $(".checkBox input[type=checkbox]").each(function(){
                        $(this).attr("checked", false);
                    });
                }
            });

        });

        /**
         * 验证原密码是否正确
         */
        function checkPwdById() {

            var pwd1=$("#password1").val();
            var t_id=$("#user_id").val();
            $.ajax({
                url:"/his/checkPwdById",//要请求的服务器url
                data:{id:t_id,password:pwd1},//第一个name对应的是后端request.getParameter("name")的name、第二个name对应的是此js中的var name = $("#name").val();的name
                async:true,//是否是异步请求
                cache:false,//是否缓存结果
                type:"POST",//请求方式
                dataType:"text",//服务器返回什么类型数据 text xml javascript json(javascript对象)
                success:function(result){//函数会在服务器执行成功后执行，result就是服务器返回结果
                        if(result=="0"){
                            console.log("result="+result)
                            alert("原密码输入错误")
                            $("#password1").val("")
                            $("#password1").focus()
                        }

                }

            });




        }

        /**
         * 判断两次输入的密码是否正确
         */
        function checkPwd(){
            var pwd2=$("#password2").val();
            var pwd3=$("#password3").val();
            if (pwd2!=pwd3){
                console.log("pwd2="+pwd2+"pwd3="+pwd3);
                alert("两次输入的密码不同")
                $("#password3").val("")
                $("#password3").focus()
            }
            if (pwd2==null || pwd2=="" || pwd3==null || pwd3=="" ){
                alert("密码不能为空")
            }

        }

        /**
         * 判断新旧密码是否相同
         */
        function checkPwd2() {
            var pwd1=$("#password1").val()
            var pwd2=$("#password2").val()
            if(pwd2==pwd1){
                alert("新旧密码相同，请重新输入")
                $("#password2").val("")
                $("#password2").focus()
            }


        }
    </script>
    <style type="text/css">
        body {
            background:#FFF
        }
    </style>
</head>

<body>
<form method="post" action="<%=basePath%>userpwdupdateController" method="post" name="ThisForm">
    <input id="user_id" type="hidden" name="user_id" value="${t_user.id}">
    <div id="contentWrap">
        <!--表格控件 -->
        <div id="widget table-widget">
            <div class="pageTitle">密码修改</div>
            <div class="pageInfo">
                <table>
                    <tr>
                        <td width="20%" align="right">原始密码</td>
                        <td width="20%"><input type="password" id="password1" name="password1" onchange="checkPwdById();" /></td>
                    </tr>
                    <tr>
                        <td width="20%" align="right">新密码</td>
                        <td width="20%"><input type="password" id="password2" name="password2" onchange="checkPwd2();"/></td>
                    </tr>
                    <tr>
                        <td width="20%" align="right">密码确认</td>
                        <td width="20%"><input type="password" id="password3" name="password3" onchange="checkPwd();" /></td>
                    </tr>
                    <tr>
                        <td colspan="2"  align="center"><input id="submin" type="submit" value="确定" /></td>
                    </tr>
                </table>
            </div>
        </div><!-- #widget -->
    </div>

</form>
</body>

</html>



