<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>用户修改页面</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript">
        $(function() {
            $('tbody tr:odd').addClass("trLight");

            $(".select-all").click(function() {
                if ($(this).attr("checked")) {
                    $(".checkBox input[type=checkbox]").each(function() {
                        $(this).attr("checked", true);
                    });
                } else {
                    $(".checkBox input[type=checkbox]").each(function() {
                        $(this).attr("checked", false);
                    });
                }
            });

        })
        function checkUser(){
            var username = $("#username").val();
            $.ajax({
                url:"<%=basePath%>checkUser?username="+username,
                dataType : "json",
                //编码设置
                contentType : "application/json;charset=utf-8",
                success : function(data) {
                    if (data.msg != null) {
                        alert(data.msg);
                        $("#username").val("");
                        $("#username").focus();
                    }
                },
                error:function(){
                    alert("后台服务器出错，请联系系统管理员！");
                }
            })
        }
        function goback(){
            window.location.href = "<%=basePath%>userList";
        }
        /**
         * 检查输入的数据是否有为空的
         */
        function checkValue() {
            var username=$("#username").val()
            var password=$("#password").val()
            var realname=$("#realname").val()
            if(username==null || username == ""){
                alert("用户名为空")
                $("#username").focus()
            }
            if(password==null || password == ""){
                alert("密码为空")
                $("#password").focus()
            }
            if(realname==null || realname == ""){
                alert("真实姓名为空")
                $("#realname").focus()
            }


        }
    </script>
    <style type="text/css">
        body {
            background: #FFF
        }
    </style>
</head>
<body>
<form method="post" action="<%=basePath%>userUpdateController" method="post" name="form1">
    <input type="hidden" id="id" name="id" value="${user.id }" />
    <div id="contentWrap">
        <!--表格控件 -->
        <div id="widget table-widget">
            <div class="pageTitle">用户修改</div>
            <font color="green" size="14">${success }</font>
            <div class="pageInfo">
                <table>
                    <tr>
                        <td width="20%" align="right">用户名</td>
                        <td width="20%"><input type="text" id="username"
                                               name="username" value="${user.username }" onchange="checkUser()"/>
                            <font id="usernameInfo"></font>
                        </td>
                        <td width="20%" align="right">真实姓名</td>
                        <td width="20%"><input type="text" id="realname"
                                               name="realname" value="${user.realname }"/></td>
                    </tr>
                    <tr>
                        <td width="10%" align="right">性别</td>
                        <td width="50%"><select id="sex" name="sex">
                            <option value="">-请选择-</option>
                            <option value="0" <c:if test="${'0' eq user.sex }">selected </c:if>>女</option>
                            <option value="1" <c:if test="${'1' eq user.sex }">selected </c:if>>男</option>
                        </select></td>

                        <td width="10%" align="right">用户类型</td>
                        <td width="50%"><select id="role" name="role">
                            <option value="">-请选择-</option>
                            <option value="01" <c:if test="${'01' eq user.role }">selected </c:if>>管理人员</option>
                            <option value="02" <c:if test="${'02' eq user.role }">selected </c:if>>服务台员工</option>
                            <option value="03" <c:if test="${'03' eq user.role }">selected </c:if>>药剂师</option>
                        </select></td>
                    </tr>
                    <tr>
                        <td width="10%" align="right">联系电话</td>
                        <td width="50%"><input type="text" id="tel" name="tel" value="${user.tel }"/></td>
                        <td width="20%" align="right">年龄</td>
                        <td width="20%"><input type="text" id="age" name="age" value="${user.age }"/></td>
                    </tr>
                    <tr>
                        <td width="10%" align="right">家庭住址</td>
                        <td width="50%"><input type="text" id="address"
                                               name="address" value="${user.address }" onchange="checkValue()"/></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <c:if test="${'1' ne flag }">
                                <input type="submit"   value="确定" />
                            </c:if>
                            <input type="button"	value="返回" onclick="goback()"/></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</form>
</body>
</html>