<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引入标签库--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>无标题文档</title>
    <link href="<%=basePath%>css/style.css" rel="stylesheet" type="text/css" />
    <link href="<%=basePath%>css/select.css" rel="stylesheet" type="text/css" />
    <link href="<%=basePath%>css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<%=basePath%>js/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>js/select-ui.min.js"></script>
    <script type="text/javascript">
        //修改用户状态的函数
        function changeStatus(id,status){
            if(status=='1'){
                if (confirm("您确定要禁用该用户吗?")){
                    document.form1.action="<%=basePath%>changeStatus?id="+id+"&sts=0";
                    document.form1.submit();
                }
            }else{
                if (confirm("您确定启用该用户吗?")){
                    document.form1.action="<%=basePath%>changeStatus?id="+id+"&sts=1";
                    document.form1.submit();
                }
            }
        }
        //修改用户的信息的函数
        function modify(id){
            if (confirm("您确定要修改此用户信息吗?")){
                window.location.href = "<%=basePath%>modifyUserController?id="+id;
            }
        }
        function gotopage(pn){
            document.form1.action="<%=basePath%>userlistController?pn="+pn;
            document.form1.submit();
        }
        $(function(){
            $('tbody tr:odd').addClass("trLight");

            $(".select-all").click(function(){
                if($(this).attr("checked")){
                    $(".checkBox input[type=checkbox]").each(function(){
                        $(this).attr("checked", true);
                    });
                }else{
                    $(".checkBox input[type=checkbox]").each(function(){
                        $(this).attr("checked", false);
                    });
                }
            });

        });
    </script>
    <style type="text/css">
        body {
            background:#FFF
        }
    </style>
</head>

<body>
<div id="contentWrap">
    <form action="<%=basePath%>userlistController" method="post" name="form1">
    <!--表格控件 -->
    <div id="widget table-widget">
        <div class="pageTitle">用户管理</div>
        <div class="querybody">
            <ul class="seachform">
                <li><label>用户ID</label><input name="id" type="text" class="scinput" value="${id}"  /></li>
                <li><label>用户名称</label><input name="username" type="text" class="scinput" value="${username}"/></li>
                <li><label>用户类型</label>
                    <select style="width:150px;height:32px;" name="role">
                        <option value="">-请选择-</option>
                        <c:if test="${srole =='01'}"> <option value="01" selected>管理人员</option></c:if>
                        <c:if test="${srole !='01'}"> <option value="01" >管理人员</option></c:if>
                        <c:if test="${srole =='02'}">  <option value="02" selected>服务台员工</option></c:if>
                        <c:if test="${srole !='02'}">  <option value="02" >服务台员工</option></c:if>
                        <c:if test="${srole !='03'}"> <option value="03">药剂师</option></c:if>
                        <c:if test="${srole =='03'}"> <option value="03" selected>药剂师</option></c:if>
                    </select>
                </li>
                <li><label>&nbsp;</label><input name="" type="submit" class="scbtn" value="查询"/></li>
            </ul>
        </div>
        <div class="pageColumn">
            <div class="pageButton"><a href="<%=basePath%>user/userAdd.jsp"><img src="<%=basePath%>images/t01.png" title="新增"/></a><span>用户列表</span></div>
            <table>
                <thead>
                <th width="">用户ID</th>
                <th width="">用户名</th>
                <th width="">姓名</th>
                <th width="">用户类型</th>
                <th width="">联系电话</th>
                <th width="">年龄</th>
                <th width="">家庭住址</th>
                <th width="">状态</th>
                <th width="10%">操作</th>
                </thead>
                <tbody>
                <c:forEach items="${pageInfo.list}" var="user">
                    <tr>
                        <td>${user.id}</td>
                        <td>${user.username}</td>
                        <td>${user.realname}</td>
                        <td>
                            <c:if test="${user.role =='01'}">管理员</c:if>
                            <c:if test="${user.role =='02'}">服务台员工</c:if>
                            <c:if test="${user.role =='03'}">药剂师</c:if>
                        </td>
                        <td>${user.tel}</td>
                        <td>${user.age}</td>
                        <td>${user.address}</td>
                        <td><c:if test="${user.status =='1' }">启用</c:if> <c:if
                                test="${user.status =='0' }">禁用</c:if></td>
                        <td>

                            <a onclick="modify('${user.id}')"><img src="<%=basePath%>images/icon/edit.png" width="16" height="16" /></a>
                            <c:if test="${user.status=='1' }">
                                <a onclick="changeStatus('${user.id }','1')"><img src="images/icon/dis.png" width="16" height="16" /></a>
                            </c:if>
                            <c:if test="${user.status=='0' }">
                                <a onclick="changeStatus('${user.id }','0')"><img src="images/icon/on.png"  width="16" height="16" /></a>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>

                </tbody>
            </table>
        </div>
    </div><!-- #widget -->
    <div id="pagination" style="align:right;margin-top:-10px;">
        <div id='project_pagination' class="pagination pagination-centered">
            <ul>
                <li ><a href="javascript:void(0);" onclick="gotopage(1)">首页</a></li>
                <li><a href="javascript:void(0);" onclick="gotopage(${pageInfo.pageNum-1})">上一页</a></li>
                <li><a href="javascript:void(0);" onclick="gotopage(${pageInfo.pageNum+1})">下一页</a></li>
                <li><a href="javascript:void(0);" onclick="gotopage(${pageInfo.pageSize})">尾页</a></li>
            </ul>
            <ul>
                <li><span>显示条数&nbsp;:&nbsp;${pageInfo.total}</span></li>
                <li><span>&nbsp;当前页数&nbsp;:&nbsp;${pageInfo.pageNum}</span></li>
            </ul>
        </div>
    </div>
    </form>
</div>
</body>
</html>
