<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>无标题文档</title>
    <link href="<%=basePath%>css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=basePath%>js/jquery.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $('tbody tr:odd').addClass("trLight");
            $(".select-all").click(function(){
                if($(this).attr("checked")){
                    $(".checkBox input[type=checkbox]").each(function(){
                        $(this).attr("checked", true);
                    });
                }else{
                    $(".checkBox input[type=checkbox]").each(function(){
                        $(this).attr("checked", false);
                    });
                }
            });
        });

        /**
         * 检查用户名和密码是否重复
         */
        function checkName() {
            var username=$("#username").val()
            console.log(username)
            $.ajax({
                url:"/his/checkName",//要请求的服务器url
                data:{username:username},//第一个name对应的是后端request.getParameter("name")的name、第二个name对应的是此js中的var name = $("#name").val();的name
                async:true,//是否是异步请求
                cache:false,//是否缓存结果
                type:"POST",//请求方式
                dataType:"text",//服务器返回什么类型数据 text xml javascript json(javascript对象)
                success:function(result){//函数会在服务器执行成功后执行，result就是服务器返回结果
                    if(result!="0"){
                        console.log("result="+result)
                        alert("用户名以存在，请重新输入用户名")
                        $("#username").val("")
                        $("#username").focus()
                    }


                }

            });

        }

        /**
         * 检查输入的数据是否有为空的
         */
        function checkValue() {
            var username=$("#username").val()
            var password=$("#password").val()
            var realname=$("#realname").val()
            if(username==null || username == ""){
                alert("用户名为空")
                $("#username").focus()
            }
            if(password==null || password == ""){
                alert("密码为空")
                $("#password").focus()
            }
            if(realname==null || realname == ""){
                alert("真实姓名为空")
                $("#realname").focus()
            }


        }
    </script>
    <style type="text/css">
        body {
            background:#FFF
        }
    </style>
</head>
<body>
<form method="post" action="<%=basePath%>addUserController" method="post" name="ThisForm">
    <div id="contentWrap">
        <!--表格控件 -->
        <div id="widget table-widget">
            <div class="pageTitle">用户添加</div>
            <div class="pageInfo">
                <label id="message">${success}</label>
                <table >
                    <tr>
                        <td width="20%" align="right">用户名</td>
                        <td width="20%"><input type="text" id="username" name="username" value="${user.username}" onchange="checkName();"/></td>
                        <td width="10%" align="right">用户密码</td>
                        <td width="50%"><input type="password" id="password" name="password" value="${user.password}" /></td>
                    </tr>
                    <tr>
                        <td width="20%" align="right">姓名</td>
                        <td width="20%"><input type="text" id="realname" name="realname" value="${user.realname}"/></td>
                        <td width="10%" align="right">用户类型</td>
                        <td width="50%">
                            <select id="type" name="role" >
                                <option value="">-请选择-</option>
                                <c:if test="${user.role=='01'}"> <option value="01" selected>管理人员</option></c:if>
                                <c:if test="${user.role!='01'}"><option value="01">管理人员</option></c:if>
                                <c:if test="${user.role!='02'}"><option value="02">服务台员工</option></c:if>
                                <c:if test="${user.role=='02'}"><option value="02" selected>服务台员工</option></c:if>
                                <c:if test="${user.role=='03'}"> <option value="03" selected>药剂师</option></c:if>
                                <c:if test="${user.role!='03'}"> <option value="03">药剂师</option></c:if>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="10%" align="right">联系电话</td>
                        <td width="50%"><input type="text" id="tel" name="tel" value="${user.tel}"/></td>
                        <td width="20%" align="right">年龄</td>
                        <td width="20%"><input type="text" id="age" name="age" value="${user.age}"/></td>
                    </tr>
                    <tr>
                        <td width="20%" align="right">性别</td>
                        <td width="20%">
                            <select id="depart" name="sex" >
                                <option value="">请选择</option>
                                <c:if test="${user.sex=='1'}"> <option value="1" selected>男</option></c:if>
                                <c:if test="${user.sex!='1'}"> <option value="1" >男</option></c:if>
                                <c:if test="${user.sex=='0'}"> <option value="0" selected>女</option></c:if>
                                <c:if test="${user.sex!='0'}"> <option value="0" >女</option></c:if>
                            </select>
                        </td>
                        <td width="10%" align="right">家庭住址</td>
                        <td width="50%"><input type="text" id="address" name="address"  value="${user.address}" onchange="checkValue();"/></td>

                    </tr>
                    <tr>
                        <c:if test="${flag!='1'}">
                            <td colspan="4" align="center"><input type="submit" value="确定"/> </td>
                        </c:if>

                    </tr>
                </table>
            </div>
        </div><!-- #widget -->
    </div>
</form>
</body>
</html>