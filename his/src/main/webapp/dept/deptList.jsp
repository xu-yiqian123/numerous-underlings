<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>科室列表</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript">

        function changeStatus(id){
            if (confirm("您确定要删除此科室嘛?")){
                window.location.href="<%=basePath%>changeStatusDept?departid="+id;
            }

        }

        function modify(id){
            if (confirm("您确定要修改此科室信息吗?")){
                window.location.href = "<%=basePath%>modifyDeptController?departid="+id;
            }

        }
    </script>
    <style type="text/css">
        body {
            background:#FFF
        }
    </style>
</head>
<body>
<div id="contentWrap">
    <!--表格控件 -->
    <form action="<%=basePath%>deptListController" method="post" name="form1">
        <div id="widget table-widget">
            <div class="pageTitle">科室管理</div>
            <div class="querybody">
                <ul class="seachform">
                    <li><label>科室名称</label><input name="name" type="text" class="scinput" value="${name }"/></li>
                    <li><label>&nbsp;</label><input name="" type="submit" class="scbtn" value="查询"/></li>
                    <li><label>&nbsp;</label><input name="" type="reset" class="scbtn" value="清空"/></li>
                </ul>
            </div>
            <div class="pageColumn">
                <div class="pageButton"><a href="<%=basePath%>dept/deptAdd.jsp"><img src="images/t01.png" title="新增"/></a><span>科室列表</span></div>
                <table>
                    <thead>
                    <th width="">科室ID</th>
                    <th width="">科室名称</th>
                    <th width="">添加人员</th>
                    <th width="">添加日期</th>
                    <th width="10%">操作</th>
                    </thead>
                    <tbody>
                    <c:forEach items="${deptList}" var="dept">
                        <tr>
                            <td>${dept.departid }</td>
                            <td>${dept.name }</td>
                            <td>${dept.createName }</td>
                            <td>
                                <fmt:formatDate value="${dept.createdate}" pattern="yyyy-MM-dd"/>
                            </td>
                            <td>
                                <a onclick="modify('${dept.departid }')"><img src="images/icon/edit.png" width="16" height="16" /></a>
                                <a onclick="changeStatus('${dept.departid }')"><img src="images/icon/del.png" width="16" height="16" /></a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div><!-- #widget  -->

    </form>
</div>
</body>
</html>