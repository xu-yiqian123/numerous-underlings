<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引入标签库--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>无标题文 档</title>
    <link href="<%=basePath%>css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=basePath%>js/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>js/rl.js"></script>
    <script type="text/javascript">
        $(function(){
            $('tbody tr:odd').addClass("trLight");

            $(".select-all").click(function(){
                if($(this).attr("checked")){
                    $(".checkBox input[type=checkbox]").each(function(){
                        $(this).attr("checked", true);
                    });
                }else{
                    $(".checkBox input[type=checkbox]").each(function(){
                        $(this).attr("checked", false);
                    });
                }
            });

        });
    </script>
    <style type="text/css">
        body {
            background:#FFF
        }
    </style>
</head>

<body>
<form method="post" action="<%=basePath%>addDrugsController" method="post" name="ThisForm">
    <div id="contentWrap">
        <!--表格控件 -->
        <div id="widget table-widget">
            <div class="pageTitle">药品添加</div>
            <div class="pageInfo">
                <label id="message">${success}</label>
                <table>

                    <tr>
                        <td width="20%" align="right">药品名</td>
                        <td width="20%"><input type="text" id="name" name="drugs_name" /></td>
                        <td width="10%" align="right">进价</td>
                        <td width="50%"><input type="text" id="purchaseprice" name="drugs_in_price" /></td>
                    </tr>
                    <tr>
                        <td width="20%" align="right">售价</td>
                        <td width="20%"><input type="text" id="price" name="drugs_sale_price" /></td>
                        <td width="10%" align="right">库存数量</td>
                        <td width="50%"><input type="text" id="num" name="drugs_num" /></td>
                    </tr>
                    <tr>
                        <td width="20%" align="right">生产厂商</td>
                        <td width="20%"><input type="text" id="productunit" name="drugs_manufacturer" /></td>
                        <td width="10%" align="right">生产日期</td>
                        <td width="50%"><input type="text" id="drugs_create_date" name="drugs_create_date" onClick="SelectDate(this.name)"/></td>
                    </tr>
                    <tr>
                        <td width="20%" align="right">保质期</td>
                        <td width="20%"><input type="text" id="drugs_quality_date" name="drugs_quality_date" onClick="SelectDate(this.name)"/></td>
                        <td width="10%" align="right">经销商</td>
                        <td width="50%"><input type="text" id="supplyunit" name="drugs_supplier" /></td>
                    </tr>
                    <c:if test="${flag!='1'}">
                        <td colspan="4" align="center"><input type="submit" value="确定"/> </td>
                    </c:if>
                </table>
            </div>
        </div><!-- #widget -->
    </div>
</form>
</body>
</html>
