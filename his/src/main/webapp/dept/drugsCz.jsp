<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>无标题文档</title>
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=basePath%>js/jquery.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $('tbody tr:odd').addClass("trLight");

            $(".select-all").click(function(){
                if($(this).attr("checked")){
                    $(".checkBox input[type=checkbox]").each(function(){
                        $(this).attr("checked", true);
                    });
                }else{
                    $(".checkBox input[type=checkbox]").each(function(){
                        $(this).attr("checked", false);
                    });
                }
            });
        });
        function goback(){
            window.location.href = "<%=basePath%>drugsList";
        }
    </script>
    <style type="text/css">
        body {
            background:#FFF
        }
    </style>
</head>

<body>
<form method="post" action="<%=basePath%>drugsUpdateNumController" method="post" name="ThisForm">
    <input type="hidden" id="drugs_id" name="drugs_id" value="${drugs.drugs_id }" />
    <div id="contentWrap">
        <!--表格控件 -->
        <div id="widget table-widget">
            <div class="pageTitle">药品库存新增</div>
            <div class="pageColumn">
                <div class="pageInfo">
                    <label id="message">${success}</label>
                    <table>
                        <tr>
                            <td width="">新增数量</td>
                            <td width=""><input type="text" id="drugs_num" name="drugs_num" value="${drugs.drugs_num}" /></td>
                        <tr>
                            <td colspan="4" align="center">
                                <c:if test="${'1' ne flag }">
                                    <input type="submit"   value="确定" />
                                </c:if>
                                <input type="button"	value="返回" onclick="goback()"/></td>
                        </tr>
                        </tr>

                    </table>
                </div>
            </div><!-- #widget -->
        </div>
    </div>
</form>
</body>
</html>


