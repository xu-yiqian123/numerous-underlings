<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>部门添加</title>
    <link href="<%=basePath%>css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=basePath%>js/jquery.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $('tbody tr:odd').addClass("trLight");

            $(".select-all").click(function(){
                if($(this).attr("checked")){
                    $(".checkBox input[type=checkbox]").each(function(){
                        $(this).attr("checked", true);
                    });
                }else{
                    $(".checkBox input[type=checkbox]").each(function(){
                        $(this).attr("checked", false);
                    });
                }
            });

        });
        function goback(){
            window.location.href = "<%=basePath%>deptListController";
        }
        function checkName(){
            var name =$("#name").val()
            console.log("name======"+name)
            $.ajax({
                url:"/his/checkDeptName",//要请求的服务器url
                data:{name:name},//第一个name对应的是后端request.getParameter("name")的name、第二个name对应的是此js中的var name = $("#name").val();的name
                async:true,//是否是异步请求
                cache:false,//是否缓存结果
                type:"POST",//请求方式
                dataType:"text",//服务器返回什么类型数据 text xml javascript json(javascript对象)
                success:function(result){//函数会在服务器执行成功后执行，result就是服务器返回结果
                    if(result!="0"){
                        console.log("result="+result)
                        alert("科室户名以存在，请重新输入用户名")
                        $("#name").val("")
                        $("#name").focus()
                    }


                }

            });
        }
    </script>
    <style type="text/css">
        body {
            background:#FFF
        }
        #message{
            font-style: italic;
            font-size: larger;
            color:green;
        }
    </style>
</head>
<body>
<form method="post" action="<%=basePath%>deptUpdateController" method="post" name="ThisForm">
    <input type="hidden" name="departid" value="${dept.departid}">
    <div id="contentWrap">
        <!--表格控件 -->
        <div id="widget table-widget">
            <div class="pageTitle">科室添加</div>
            <label id="message">${success}</label>
            <div class="pageInfo">
                <table>
                    <tr>
                        <td width="10%" align="right">科室名称</td>
                        <td width="50%"><input type="text" id="name" name="name" value="${dept.name}" onblur="checkName()"/></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <c:if test="${flag!='1'}">
                                <input type="submit" value="确定" />
                            </c:if>
                            &nbsp;
                            <input type="button" value="返回" onclick="goback()"/>
                        </td>
                    </tr>
                </table>
            </div>
        </div><!-- #widget -->
    </div>

</form>
</body>
</html>
