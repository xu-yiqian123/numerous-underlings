<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引入标签库--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>无标题文档</title>
    <link href="<%=basePath%>css/style.css" rel="stylesheet" type="text/css" />
    <link href="<%=basePath%>css/select.css" rel="stylesheet" type="text/css" />
    <link href="<%=basePath%>css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<%=basePath%>js/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>js/select-ui.min.js"></script>
    <script type="text/javascript">
        function gotopage(pn){
            document.form1.action="<%=basePath%>drugsListController?pn="+pn;
            document.form1.submit();
        }
        $(function(){
            $('tbody tr:odd').addClass("trLight");

            $(".select-all").click(function(){
                if($(this).attr("checked")){
                    $(".checkBox input[type=checkbox]").each(function(){
                        $(this).attr("checked", true);
                    });
                }else{
                    $(".checkBox input[type=checkbox]").each(function(){
                        $(this).attr("checked", false);
                    });
                }
            });

        });
        //修改药品信息的函数
        function deletes(id){
            if (confirm("您确定要删除此药品信息吗?")){
                window.location.href = "<%=basePath%>delDrugsController?id="+id;
            }

        }
        //修改用户的信息的函数
        function modify(id){
            if (confirm("您确定要修改此药品信息吗?")){
                window.location.href = "<%=basePath%>modifyDrugsController?id="+id;
            }
        }
        //增加药品库存数
        function cz(id) {
            if (confirm("您确定要增加此药品的库存数吗?")){
                window.location.href = "<%=basePath%>czDrugsController?id="+id;
            }
        }
    </script>
    <style type="text/css">
        body {
            background:#FFF
        }
    </style>
</head>

<body>
<div id="contentWrap">
    <form action="<%=basePath%>drugsListController" method="post" name="form1">
    <!--表格控件 -->
    <div id="widget table-widget">
        <div class="pageTitle">药品管理</div>
        <div class="querybody">
            <ul class="seachform">
                <li><label>药品ID</label><input name="drugs_id" type="text" class="scinput" value="${drugs_id}"  /></li>
                <li><label>用户名称</label><input name="drugs_name" type="text" class="scinput" value="${drugs_name}"/></li>
                <li><label>&nbsp;</label><input name="" type="submit" class="scbtn" value="查询"/></li>
            </ul>
        </div>
        <div class="pageColumn">
            <div class="pageButton"><a href="<%=basePath%>drugs/drugsAdd.jsp"><img src="<%=basePath%>images/t01.png" title="新增"/></a><span>用户列表</span></div>
            <table>
                <thead>
                <th width="">药品ID</th>
                <th width="">药品名称</th>
                <th width="">进价</th>
                <th width="">售价</th>
                <th width="">库存数量</th>
                <th width="">引入日期</th>
                <th width="">生产厂商</th>
                <th width="">生产日期</th>
                <th width="">保质期</th>
                <th width="">供货单位</th>
                <th width="10%">操作</th>
                <th width="10%">库存新增</th>
                </thead>
                <tbody>
                <c:forEach items="${pageInfo.list}" var="drugs">
                    <tr>
                        <td>${drugs.drugs_id}</td>
                        <td>${drugs.drugs_name}</td>
                        <td>${drugs.drugs_in_price}</td>
                        <td>${drugs.drugs_sale_price}</td>
                        <td>${drugs.drugs_num}</td>
                        <td>
                            <fmt:formatDate value="${drugs.drugs_in_date}" pattern="yyyy-MM-dd"/>
                        </td>

                        <td>${drugs.drugs_manufacturer}</td>
                        <td>
                            <fmt:formatDate value="${drugs.drugs_create_date}" pattern="yyyy-MM-dd"/>
                        </td>
                        <td>
                            <fmt:formatDate value="${drugs.drugs_quality_date}" pattern="yyyy-MM-dd"/>
                        </td>
                        <td>${drugs.drugs_supplier}</td>
                        <td>
                            <a onclick="modify('${drugs.drugs_id}')"><img src="<%=basePath%>images/icon/edit.png" width="16" height="16" /></a>
                            <a onclick="deletes('${drugs.drugs_id}')"><img src="<%=basePath%>images/icon/del.png" width="16" height="16" /></a>
                        </td>
                        <td>
                            <a onclick="cz('${drugs.drugs_id}')"><img src="<%=basePath%>images/icon/edit2.png" width="16" height="16" /></a>
                        </td>
                        </td>
                    </tr>
                </c:forEach>

                </tbody>
            </table>
        </div>
    </div><!-- #widget -->
    <div id="pagination" style="align:right;margin-top:-10px;">
        <div id='project_pagination' class="pagination pagination-centered">
            <ul>
                <li ><a href="javascript:void(0);" onclick="gotopage(1)">首页</a></li>
                <li><a href="javascript:void(0);" onclick="gotopage(${pageInfo.pageNum-1})">上一页</a></li>
                <li><a href="javascript:void(0);" onclick="gotopage(${pageInfo.pageNum+1})">下一页</a></li>
                <li><a href="javascript:void(0);" onclick="gotopage(${pageInfo.pageSize})">尾页</a></li>
            </ul>
            <ul>
                <li><span>显示条数&nbsp;:&nbsp;${pageInfo.total}</span></li>
                <li><span>&nbsp;当前页数&nbsp;:&nbsp;${pageInfo.pageNum}</span></li>
            </ul>
        </div>
    </div>
    </form>
</div>
</body>
</html>
