<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";

%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>无标题文档</title>
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=basePath%>js/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>js/rl.js"></script>
    <script type="text/javascript">
        $(function(){
            $('tbody tr:odd').addClass("trLight");

            $(".select-all").click(function(){
                if($(this).attr("checked")){
                    $(".checkBox input[type=checkbox]").each(function(){
                        $(this).attr("checked", true);
                    });
                }else{
                    $(".checkBox input[type=checkbox]").each(function(){
                        $(this).attr("checked", false);
                    });
                }
            });
        });
        function goback(){
            window.location.href = "<%=basePath%>drugsList";
        }
    </script>
    <style type="text/css">
        body {
            background:#FFF
        }
    </style>
</head>

<body>
<form method="post" action="<%=basePath%>drugsUpdateController" method="post" name="ThisForm">
    <input type="hidden" id="drugs_id" name="drugs_id" value="${drugs.drugs_id }" />
    <div id="contentWrap">
        <!--表格控件 -->
        <div id="widget table-widget">
            <div class="pageTitle">药品信息修改</div>
            <div class="pageInfo">
                <label id="message">${success}</label>
                <table>
                    <tr>
                        <td width="20%" align="right">药品名</td>
                        <td width="20%"><input type="text" id="name" name="drugs_name" value="${drugs.drugs_name}"/></td>
                        <td width="10%" align="right">进价</td>
                        <td width="50%"><input type="text" id="purchaseprice" name="drugs_in_price" value="${drugs.drugs_in_price}" /></td>
                    </tr>
                    <tr>
                        <td width="20%" align="right">售价</td>
                        <td width="20%"><input type="text" id="price" name="drugs_sale_price"  value="${drugs.drugs_sale_price}"/></td>
                        <td width="10%" align="right">库存数量</td>
                        <td width="50%"><input type="text" id="num" name="drugs_num" value="${drugs.drugs_num}" /></td>
                    </tr>
                    <tr>
                        <td width="20%" align="right">生产厂商</td>
                        <td width="20%"><input type="text" id="productunit" name="drugs_manufacturer" value="${drugs.drugs_manufacturer}" /></td>
<%--                        <td width="10%" align="right">生产日期</td>--%>
<%--                        <td width="50%"><input type="text" id="drugs_create_date" name="drugs_create_date" onClick="SelectDate(this.name)" value="<fmt:formatDate value="${drugs.drugs_create_date}" pattern="yyyy-MM-dd"/>" /></td>--%>
                        <td width="10%" align="right">生产日期</td>
                        <td width="50%"><input type="text" id="drugs_create_date" name="drugs_create_date" onClick="SelectDate(this.name)" value="${drugs.drugs_create_date}" /></td>

                    </tr>
                    <tr>
<%--                        <td width="20%" align="right">保质期</td>--%>
<%--                        <td width="20%"><input type="text" id="drugs_quality_date" name="drugs_quality_date" onClick="SelectDate(this.name)" value="<fmt:formatDate value="${drugs.drugs_quality_date}" pattern="yyyy-MM-dd"/>" /></td>--%>
                            <td width="20%" align="right">保质期</td>
                            <td width="20%"><input type="text" id="drugs_quality_date" name="drugs_quality_date" onClick="SelectDate(this.name)" value="${drugs.drugs_quality_date}" /></td>

    <td width="10%" align="right">经销商</td>
                        <td width="50%"><input type="text" id="supplyunit" name="drugs_supplier" value="${drugs.drugs_supplier}"/></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <c:if test="${'1' ne flag }">
                                <input type="submit"   value="确定" />
                            </c:if>
                            <input type="button"	value="返回" onclick="goback()"/></td>
                    </tr>
                </table>
            </div>
        </div><!-- #widget -->
    </div>
</form>
</body>
</html>